import styled from 'styled-components'

export const PostsList = styled.section`
    width: 95%;
    margin-bottom: 15px;
`